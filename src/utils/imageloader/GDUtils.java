package utils.imageloader;


import android.content.Context;
import ru.recoilme.ivk.AndroidApplication;

import java.util.concurrent.ExecutorService;

/**
 * Class that provides several utility methods related to GreenDroid.
 * 
 * @author Cyril Mottier
 */
public class GDUtils {

    private GDUtils() {
    }

    /**
     * 
     * @param context The calling context
     */
    public static AndroidApplication getGDApplication(Context context) {
        return (AndroidApplication) context.getApplicationContext();
    }

    /**
     * 
     * @param context The calling context
     */
    public static ImageCache getImageCache(Context context) {
        return getGDApplication(context).getImageCache();
    }

    /**
     * 
     * @param context The calling context
     */
    public static ExecutorService getExecutor(Context context) {
        return getGDApplication(context).getExecutor();
    }

}
