package utils.asynctaskloader;

public interface OnTaskCompleteListener {
    // Notifies about task completeness
    void onTaskComplete(NetTask task);
}

